package com.service;

import com.model.User;

public interface UserService {

	public abstract boolean checkUser(User user);
}
