package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dao.UserDao;
import com.model.User;

@Repository("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	public boolean checkUser(User user) {
		
		if(user!=null) {
			boolean value=userDao.checkUser(user);
			if(value==true)
				return true;
		}
		return false;
	}

	
}
