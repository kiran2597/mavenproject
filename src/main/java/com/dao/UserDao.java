package com.dao;

import com.model.User;

public interface UserDao {

	public boolean checkUser(User user);
}
