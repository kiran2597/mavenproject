package com.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.User;



@Repository("userDao")
public class UserDaoImpl implements UserDao{

	
	@Autowired
	private SessionFactory sessionFactory;
	

	Session session =null;
	Long userId;
	public Session getSession() {
		try {
			session = sessionFactory.getCurrentSession();
		}catch(Exception e) {
			session = sessionFactory.openSession();
		}
		return session;
	}
	
	
	public boolean checkUser(User user) {
		
		if(user!=null) {
			Session session = getSession();
			String hql = "from user";
			if(!hql.isEmpty()) {
				Query query = session.createQuery(hql);
				List<User> users=query.getResultList();
				for(User us:users) {
					if(us.getUsername().equals(user.getUsername()) && us.getPassword().equals(user.getPassword())) {
						return true;
					}
				}
			}
		}
		return false;
	}

}
