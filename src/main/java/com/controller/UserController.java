package com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.model.User;
import com.service.UserService;

public class UserController {

	
	@Autowired
	private UserService userService;
	
	@GetMapping("/login")
	public String loginPage(Model model) {
		model.addAttribute("user", new User());
		return "login";
	}
	
	
	@PostMapping("/authenticate")
	public String authenticateUser(@ModelAttribute User user,Model model) {
			boolean value = userService.checkUser(user);
			if(value==true) {
				model.addAttribute("message", "User is successfully logged in");
				return "messagePage";
			}else {
				model.addAttribute("message", "Invalid credentials");
				return "login";
			}
	}
	
	@GetMapping("/logout")
	public String logout(HttpServletRequest request) {
		
		HttpSession session  = request.getSession();
		session.invalidate();
		return "logoutPage";
	}
}
