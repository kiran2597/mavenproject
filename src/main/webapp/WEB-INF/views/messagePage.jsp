<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title></title>
</head>
<body>
		<div>
			<p style="text-align: right;"><a href="/logout">Logout</a></p>
		</div>
		<div align="center">
			<c:if test="${not empty message}">
				<p align="center" style="color:red">${message}</p>
			</c:if>
		</div>
</body>
</html>